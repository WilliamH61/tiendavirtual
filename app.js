const express =require('express');
const bodyParser = require('body-parser');

let app = express();
app.use( express.json());
app.use(express.urlencoded({ extended: true }));
app.use( (req,res,next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-Key,Origin, X-Requested-With, Content-Type, Accept, Accept-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'POST, PUT, DELETE, OPTIONS');
    res.header('Allow', 'get', 'POST, PUT, DELETE, OPTIONS');
    next();   
}   );

module.exports = app;